package com.senselessweb.plog.android.client.galleries;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.senselessweb.plog.android.R;
import com.senselessweb.plog.android.client.galleries.GalleryFragment.OnListFragmentInteractionListener;
import com.senselessweb.plog.android.service.domain.Album;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GalleryRecyclerViewAdapter extends RecyclerView.Adapter<GalleryRecyclerViewAdapter.ViewHolder> {

    private final List<Album> albums;
    private final OnListFragmentInteractionListener mListener;

    public GalleryRecyclerViewAdapter(List<Album> items, OnListFragmentInteractionListener listener) {
        albums = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_gallery, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = albums.get(position);
        holder.mAlbumTitle.setText(albums.get(position).getName());
        Picasso.get().load("https://senselessweb.com/plog/api/photos/" + albums.get(position).getAlbumPhoto().getId() + "/thumbnail/1024").into(holder.mAlbumPhoto);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mAlbumTitle;
        public final ImageView mAlbumPhoto;
        public Album mItem;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;
            this.mAlbumTitle = (TextView) view.findViewById(R.id.fragment_gallery_item_title);
            this.mAlbumPhoto = (ImageView) view.findViewById(R.id.fragment_gallery_item_image);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mAlbumPhoto + "'";
        }
    }
}
