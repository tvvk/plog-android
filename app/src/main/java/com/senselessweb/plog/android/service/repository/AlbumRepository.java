package com.senselessweb.plog.android.service.repository;

import com.senselessweb.plog.android.service.domain.Album;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AlbumRepository {

    @GET("albums")
    Call<List<Album>> listAlbums();
}
