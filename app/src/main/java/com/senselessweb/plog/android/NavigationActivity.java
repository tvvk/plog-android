package com.senselessweb.plog.android;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.senselessweb.plog.android.R;

public class NavigationActivity extends AppCompatActivity {

    private Fragment mapFragment;
    private Fragment albumsFragment;
    private Fragment galleriesFragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            final FragmentManager fm = getSupportFragmentManager();
            switch (item.getItemId()) {
                case R.id.navigation_map:
                    fm.beginTransaction()
                            .show(mapFragment)
                            .hide(albumsFragment)
                            .hide(galleriesFragment)
                            .commit();
                    return true;
                case R.id.navigation_albums:
                    fm.beginTransaction()
                            .hide(mapFragment)
                            .show(albumsFragment)
                            .hide(galleriesFragment)
                            .commit();
                    return true;
                case R.id.navigation_galleries:
                    fm.beginTransaction()
                            .hide(mapFragment)
                            .hide(albumsFragment)
                            .show(galleriesFragment)
                            .commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        this.mapFragment = this.getSupportFragmentManager().findFragmentById(R.id.fragment_main_map);
        this.albumsFragment = this.getSupportFragmentManager().findFragmentById(R.id.fragment_main_albums);
        this.galleriesFragment = this.getSupportFragmentManager().findFragmentById(R.id.fragment_main_galleries);

        getSupportFragmentManager().beginTransaction()
                .show(mapFragment)
                .hide(albumsFragment)
                .hide(galleriesFragment)
                .commit();


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
