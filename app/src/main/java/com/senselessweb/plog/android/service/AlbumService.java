package com.senselessweb.plog.android.service;

import com.senselessweb.plog.android.service.domain.Album;
import com.senselessweb.plog.android.service.repository.AlbumRepository;

import java.util.List;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class AlbumService {

    private final AlbumRepository repository;

    public AlbumService() {
        this.repository = new Retrofit.Builder()
                .baseUrl("https://senselessweb.com/plog/api/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(AlbumRepository.class);
    }

    public void fetchAllAlbums(Callback<List<Album>> callback) {
        this.repository.listAlbums().enqueue(callback);
    }
}
