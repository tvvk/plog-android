package com.senselessweb.plog.android.service.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Album {

    private String id;
    private String name;
    private Photo albumPhoto;
}
